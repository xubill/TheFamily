const DT = {};

DT.today = {};

// 数据库日期字符串转Date对象
DT.date = function(date) {
    if (date) {
        return new Date(date.replace(/-/g, '/'));
    }
    return new Date();
};

// 日期去0
DT.simple = function(date) {
    return date.replace(/-0/g, '-');
}

// 数字补0
DT.repair = function(num) {
    return ('0' + num).slice(-2);
}

// 日期补0
DT.full = function(date) {
    const arr = date.split('-');
    return `${arr[0]}-${DT.repair(arr[1])}-${DT.repair(arr[2])}`;
};

DT.todayStr = function() {
    const gong = DT.today.gong;
    const nong = DT.today.nong;
    const nian = DT.today.nian;
    return `${DT.getGongStr(gong)} ${DT.getNongStr(nong, nian)}[${DT.getShuXian(nian)}年]`;
};

DT.getGongStr = function (gong) {
    const date = DT.date(gong);
    return `${date.getFullYear()}年${DT.repair(date.getMonth() + 1)}月${DT.repair(date.getDate())}日${DT.getWeek(gong, date.getDay())}`
};

DT.getXinZuo = function (gong) {
    try {
        let arr = gong.split('-');
        let val = parseInt(arr[1]) * 100 + parseInt(arr[2]);
        if (val > 321 && val < 419) return '白羊座';
        else if (val >= 420 && val <= 520) return '金牛座';
        else if (val >= 521 && val <= 621) return '双子座';
        else if (val >= 622 && val <= 722) return '巨蟹座';
        else if (val >= 723 && val <= 822) return '狮子座';
        else if (val >= 823 && val <= 922) return '处女座';
        else if (val >= 923 && val <= 1023) return '天秤座';
        else if (val >= 1024 && val <= 1122) return '天蝎座';
        else if (val >= 1123 && val <= 1221) return '射手座';
        else if (val >= 1222 || val <= 119) return '魔羯座';
        else if (val >= 120 && val <= 218) return '水瓶座';
        else if (val >= 219 && val <= 320) return '双鱼座';
        else return '';
    } catch (err) {
        console.error(err);
        return '';
    }
};

const weekZh = '日一二三四五六';

DT.getWeek = function (gong, num) {
    if (num == undefined) {
        num = DT.date(gong).getDay();
    }
    if (num >= 0 && num <= 6) {
        return '星期' + weekZh.charAt(num);
    } else {
        return '星期几';
    }
};

DT.getNongStr = function (nong, nian) {
    let arr = nong.split('-');
    let yue = DT.getYue(arr[1]);
    let ri = DT.getRi(arr[2]);
    return `${nian}年${yue}月${ri}`;
};

DT.getNian = function (nian) {
    return nian.toString().split('').map(function (v) {
        return riZh.charAt(parseInt(v));
    }).join('');
};

const yueZh = ['正', '二', '三', '四', '五', '六', '七', '八', '九', '十', '十一', '腊'];

DT.getYue = function (num) {
    if (num >= 1 && num <= 12) {
        return yueZh[num - 1];
    } else {
        return '某';
    }
};

const riZh = '零一二三四五六七八九十';

DT.getRi = function (num) {
    if (num >= 1 && num <= 10) {
        return '初' + riZh.charAt(num);
    } else if (num >= 10 && num <= 19) {
        return '十' + riZh.charAt(num - 10).replace('零', '');
    } else if (num >= 20 && num <= 29) {
        return '廿' + riZh.charAt(num - 20).replace('零', '十');
    } else if (num === 30) {
        return '三十'
    } else {
        return '某';
    }
};
// 根据地支返回属相
DT.getShuXian = function (nian) {
    const dz = nian.charAt(1);
    if (dz === '子') return '鼠';
    else if (dz === '丑') return '牛';
    else if (dz === '寅') return '虎';
    else if (dz === '卯') return '兔';
    else if (dz === '辰') return '龙';
    else if (dz === '巳') return '蛇';
    else if (dz === '午') return '马';
    else if (dz === '未') return '羊';
    else if (dz === '申') return '猴';
    else if (dz === '酉') return '鸡';
    else if (dz === '戌') return '狗';
    else if (dz === '亥') return '猪';
    else return '啥';
}
// 根据今天的日期，计算农历年龄
DT.getAge = function (person) {
    const t = DT.date(DT.today.nong.replace('闰', ''));
    const b = DT.date(person.nong);
    let age = t.getFullYear() - b.getFullYear();
    if (t.getMonth() < b.getMonth() || (t.getMonth() === b.getMonth() && t.getDate() < b.getDate())) {
        age -= 1; // 如果生日还没到，减1
    }
    return age;
};

module.exports = DT;
