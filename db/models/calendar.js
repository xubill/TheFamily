const Sequelize = require('sequelize');
const conf = require('../conf');

const Calendar = conf.define('calendar', {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,
    },
    gong: {
        unique: true,
        type: Sequelize.STRING,
        length: 16,
        comment: '公历',
    },
    nong: {
        unique: true,
        type: Sequelize.STRING,
        length: 16,
        comment: '农历',
    },
    nian: {
        type: Sequelize.STRING,
        length: 4,
        comment: '农历年',
    }
}, {
    // options
});

module.exports = Calendar;