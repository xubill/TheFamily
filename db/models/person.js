const Sequelize = require('sequelize');
const conf = require('../conf');

const Person = conf.define('person', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    fid: {
        type: Sequelize.INTEGER,
        comment: '父亲ID',
    },
    mid: {
        type: Sequelize.INTEGER,
        comment: '母亲ID',
    },
    xid: {
        type: Sequelize.INTEGER,
        comment: '爱人ID',
    },
    name: {
        type: Sequelize.STRING(8),
        comment: '姓名',
    },
    sex: {
        type: Sequelize.STRING(2),
        comment: '性别',
    },
    gong: {
        type: Sequelize.STRING(10),
        comment: '公历生日，月份从1开始',
    },
    nong: {
        type: Sequelize.STRING(10),
        comment: '农历生日，月份从1开始',
    },
    nian: {
        type: Sequelize.STRING(4),
        comment: '农历年，天干地支',
    },
}, {
    // options
});

module.exports = Person;